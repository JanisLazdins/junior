<?php

use App\Core\Router;
use App\Core\Request;

require 'vendor/autoload.php';
require 'core/boot.php';

Router::load('app/routes.php')->redirect(Request::uri(), Request::method());
