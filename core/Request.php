<?php

namespace App\Core;

class Request
{

    /**
     * Trims link from unnecessary parts.
     * 
     * @return string Contains URI
     */
    public static function uri()
    {
        return trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/');
    }

    /**
     * Return method used (GET or POST).
     *
     * @return string 
     */
    public static function method()
    {
        return $_SERVER['REQUEST_METHOD'];
    }
}