<?php

namespace App\Core;

class Router
{
    /**
     * All registered routes.
     *
     * @var array
     */
    public $routes = [
        'GET' => [],
        'POST' => []
    ];

    /**
     * Load a user's routes file.
     *
     * @param string $file
     */
    public static function load($file)
    {
        $router = new static;
        require $file;
        return $router;
    }

    /**
     * Register a GET route.
     *
     * @param string $uri
     * @param string $controller
     */
    public function get($uri, $controller)
    {
        $this->routes['GET'][$uri] = $controller;
    }

    /**
     * Register a POST route.
     *
     * @param string $uri
     * @param string $controller
     */
    public function post($uri, $controller)
    {
        $this->routes['POST'][$uri] = $controller;
    }

    /**
     * Load the requested URI's associated controller method.
     *
     * @param string $uri
     * @param string $requestMethod
     */
    public function redirect($uri, $requestMethod)
    {
        if (array_key_exists($uri, $this->routes[$requestMethod])) {
            return $this->controllerLoad(
                    ...explode('@', $this->routes[$requestMethod][$uri])
            );
        }
        throw new Exception('No route has been definde for this URI');
    }

    /**
     * Load and call the relevant controller action.
     *
     * @param string $controllerName
     * @param string $action
     */
    protected function controllerLoad($controllerName, $action)
    {
        $controllerPath = "App\\Controllers\\{$controllerName}";
        $controller = new $controllerPath;
        if (method_exists($controller, $action)) {
            return $controller->$action();
        }
        throw new Exception("{$controller} does not respond to {$action} action");
    }
}