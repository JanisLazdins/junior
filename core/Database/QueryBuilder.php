<?php

/**
 * Class contains methods related to MySql.
 */
class QueryBuilder
{
    protected $pdo;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Retrieves all data from database.
     *
     * @param string $table Contains table name.
     *
     * @return array Fetch style is PDO::FETCH_CLASS.
     */
    public function getAll($table)
    {
        $statement = $this->pdo->prepare("SELECT * FROM {$table}");
        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_CLASS);
    }

    /**
     * Uploads to database with a possibility of an exception.
     * Prepares statement for execution and returns an object.
     *
     * @param string $table Contains table name.
     *
     * @param array $parameters Contains data from submitted form in 'add' view
     */
    public function insert($table, $parameters)
    {

        $sql = sprintf(
            'INSERT INTO %s (%s) VALUES (%s)', $table,
            implode(', ', array_keys($parameters)),
            ':'.implode(', :', array_keys($parameters))
        );

        try {
            $statement = $this->pdo->prepare($sql);
            $statement->execute($parameters);
        } catch (Exception $e) {
            die('Something went wrong'.$e->getMessage());
        }
    }

    /**
     * Delete function executed when the chosen option in 'inspect' views form is "mass-delete".
     * Checks if check boxes are checked and delete all checked ones.
     *
     * @param string $table Contains table name
     */
    public function deleteSelected($table, $checkbox)
    {
        $statement = $this->pdo->prepare("DELETE FROM $table WHERE id IN ("
            .implode(',', $checkbox).
            ")");
        $statement->execute();
    }

    /**
     * Counts the number of rows with the same 'sku' in the database.
     *
     * @param string $table
     * @param string $input
     * @return string
     */
    public function checkDuplicates($table, $input)
    {
        $statement = $this->pdo->prepare("SELECT * FROM $table WHERE sku = '$input'");
        $statement->execute();
        $rowCount = $statement->rowCount();
        return $rowCount;
    }
}