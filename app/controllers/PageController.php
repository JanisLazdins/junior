<?php

namespace App\Controllers;

/**
 * This class simply renders any views without anything dynamic in them
 */
class PageController extends Controller
{

    /**
     * Shows home page
     *
     * @return string
     */
    public function main()
    {
        return $this->helper->view('main');
    }
}