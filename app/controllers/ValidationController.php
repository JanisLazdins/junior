<?php

namespace App\Controllers;

use App\Core\Container;

/**
 * This class is responsible for validating users input.
 *
 */
class ValidationController extends Controller
{
    const TABLE = 'products';

    /**
     * This method contains all the input rules.
     * Variable $data is sent from "AddController" "store" method.
     *
     * @param array $data
     */
    public function rules($data)
    {
        foreach ($data as $key => $value) {
            foreach ($value as $rule => $error) {

                switch ($rule) {
                    case 'alreadyExists':
                        if (!$this->alreadyExists($_POST[$key]) == $key) {
                            $this->message[$key] = $error;
                        }
                        break;
                    case 'numeric':

                        if (!is_numeric($this->cleanNumber($_POST[$key])) == $key) {
                            $this->message[$key] = $error;
                        }
                        break;

                    case 'alphanumeric':
                        if (!ctype_alnum($_POST[$key]) == $key) {
                            $this->message[$key] = $error;
                        }
                        break;
                    case 'capitalAndNumber':
                        if (!$this->is_capitalAndNumber($this->cleanCapitalAndNumber($_POST[$key]))
                            == $key) {
                            $this->message[$key] = $error;
                        }
                        break;
                    case 'simpletext':
                        if (!$this->is_simpletext($this->cleanSimpleText($_POST[$key]))
                            == $key) {
                            $this->message[$key] = $error;
                        }
                        break;
                    case 'between':
                        foreach ($error as $between => $minmax) {
                            switch ($between) {
                                case 'min':
                                    if (strlen($_POST[$key]) < $minmax) {
                                        $this->message[$key] = $error['error'];
                                    }
                                    break;

                                case 'max':
                                    if (strlen($_POST[$key]) > $minmax) {
                                        $this->message[$key] = $error['error'];
                                    }
                                    break;
                            }
                        }
                        break;
                    case 'is_select':

                        if ($_POST[$key] == '' || $_POST[$key] == 0) {
                            $this->message[$key] = $error;
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }

    /**
     * Uses function "preg_match" and checks if submitted data contains only capital letters and numbers.
     *
     * @param string $value
     * @return boolean
     */
    private function is_capitalAndNumber($value)
    {
        if (preg_match("/^[A-Z0-9]*$/", $value)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Changes symbols to uppercase and deletes anything that is not uppercase letters or numbers.
     *
     * @param string $data
     * @return string
     */
    public function cleanCapitalAndNumber($data)
    {
        $data = preg_replace('/[^A-Z0-9]/', "", strtoupper($data));
        return $data;
    }

    /**
     * Uses built in function "preg_match" and checks if submitted data contains only letters and numbers.
     *
     * @param string $value
     * @return boolean
     */
    private function is_simpletext($value)
    {
        if (preg_match("/^[a-zA-Z0-9 .\-\,]+$/i", $value)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Replace invalid code unit sequences with a Unicode Replacement Characters.
     *
     * @param string $data
     * @return string
     */
    public function cleanSimpleText($data)
    {
        $data = htmlentities($data, ENT_QUOTES | ENT_SUBSTITUTE, "UTF-8");
        return $data;
    }

    /**
     * Strips the string from white spaces and forward slashes.
     * Replaces (,) with (.).
     * Converts quotes.
     *
     * @param string $data
     * @return string
     */
    public function cleanNumber($data)
    {
        $data = str_replace(" ", "", trim($data));
        $data = str_replace("/", "", $data);
        $data = str_replace(",", ".", $data);
        $data = htmlentities($data, ENT_QUOTES);
        return $data;
    }

    /**
     * Displays a consolidated error besides Type switcher.
     * Details of the error are displayed using "display" method besides the associated fields.
     *
     * @return string
     */
    public function inputError()
    {
        $inputValue = array(
            'disk_size',
            'book_weight',
            'furniture_height',
            'furniture_width',
            'furniture_lenght'
        );
        if (isset($_POST['addSubmit'])) {
            if (!empty($this->message[$inputValue[0]])) {
                return "Error!";
            }
            if (!empty($this->message[$inputValue[1]])) {
                return "Error!";
            }
            if (!empty($this->message[$inputValue[2]])) {
                return "Error!";
            }
            if (!empty($this->message[$inputValue[3]])) {
                return "Error!";
            }
            if (!empty($this->message[$inputValue[4]])) {
                return "Error!";
            }
        }
    }

    /**
     * Saves number of rows with the same 'sku' in variable 'rowCount'.
     * Returns true if a row with the same 'sku' already exists.
     *
     * @param string $data
     * @return boolean
     */
    public function alreadyExists($data)
    {
        $rowCount = Container::retrieve('database')->checkDuplicates(self::TABLE,
            $data);
        if ($rowCount > 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Displays error messages.
     *
     * @param string $value
     */
    public function display($value)
    {
        if (isset($_POST)) {
            if (isset($this->message[$value])) {
                return $this->message[$value];
            }
        }
    }

    /**
     * Helpers function to save submitted values in the input fields in the "add" view.
     *
     * @param string $item
     * @return type
     */
    public static function get($item)
    {
        if (isset($_POST[$item])) {
            return $_POST[$item];
        } elseif (isset($_GET[$item])) {
            return $_GET[$item];
        }
    }
}