<?php

namespace App\Controllers;

/**
 * This class contains functions that can not be grouped into one particular class
 */
class Helpers
{

    /**
     * Renders a view and provides product data.
     *
     * @param string $name Contains first part of file name
     * @param array $data Contains product data to be shown in 'inspect' view
     */
    public function view($name, $data = [])
    {
        extract($data);
        require "app/views/partials/head.php";
        require "app/views/{$name}_view.php";
    }

    /**
     * Redirect to a different link.
     *
     * @param string $path
     */
    public function redirecting($path)
    {
        header("Location: {$path}");
    }
}