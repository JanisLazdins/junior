<?php

namespace App\Controllers;

use App\Core\Container;

/**
 * This class is responsible for function avaliable in 'add' view
 */
class AddController extends Controller
{
    const TABLE = 'products';

    /**
     * Variable $add is sent to the 'add' view for further use as a route.
     * Shows the add page
     *
     * @return string URI
     */
    public function index()
    {
        $form = new ValidationController();
        $add = htmlspecialchars('store');
        return $this->helper->view('add', compact('add', 'form'));
    }

    /**
     * Method "store" sets all input rules and defines them in an array.
     * "store" method is being called repeatedly until no errors remain in the form.
     * After all the input rules have been fulfilled, method "insert" is executed.
     * Loads product list page using 'view' helper method.
     *
     * @return string URI
     */
    public function store()
    {
        $form = new ValidationController();
        if (isset($_POST['addSubmit'])) {
            $form->rules(array(
                'sku' => array(
                    'capitalAndNumber' => 'Onlu capital letters and numbers',
                    'alreadyExists' => 'Inserted SKU already exists',
                    'between' => array(
                        'min' => 3,
                        'max' => 9,
                        'error' => 'Please enter valid SKU between 3 and 9 symbols')),
                'name' => array(
                    'simpletext' => 'Symbols - ! $ ^ [ ] | < > are not allowed',
                    'between' => array(
                        'min' => 2,
                        'max' => 32,
                        'error' => 'Name must be between 2 to 32 characters')),
                'price' => array(
                    'numeric' => 'Please enter product price in EUR',
                    'between' => array(
                        'min' => 1,
                        'max' => 13,
                        'error' => 'Price too short or too long')),
                'type' => array('is_select' => 'Please select type'),
            ));
            if ($_POST['type'] == 1) {
                $form->rules(array(
                    'disk_size' => array(
                        'numeric' => 'Input valid disk size in MB',
                        'between' => array(
                            'min' => 1,
                            'max' => 10,
                            'error' => 'Disk sizebetween 1 and 10 characters')),
                ));
            } elseif ($_POST['type'] == 2) {
                $form->rules(array(
                    'book_weight' => array(
                        'numeric' => 'Input valid book weight in KG',
                        'between' => array(
                            'min' => 1,
                            'max' => 10,
                            'error' => 'Book weight must be between 1 and 10 characters')),
                ));
            } elseif ($_POST['type'] == 3) {
                $form->rules(array(
                    'furniture_height' => array(
                        'numeric' => 'Input valid height in MM',
                        'between' => array(
                            'min' => 1,
                            'max' => 10,
                            'error' => 'Furniture Height must be between 1 and 10 characters')),
                    'furniture_width' => array(
                        'numeric' => 'Input valid width in MM',
                        'between' => array(
                            'min' => 1,
                            'max' => 10,
                            'error' => 'Furniture Width must be between 1 and 10 characters')),
                    'furniture_lenght' => array(
                        'numeric' => 'Input valid lenght in MM',
                        'between' => array(
                            'min' => 1,
                            'max' => 10,
                            'error' => 'Furniture Lenght must be between 1 and 10 characters'))
                ));
            }

            if (empty($form->message)) {
                $data = array(
                    'sku' => $form->cleanCapitalAndNumber($_POST['sku']),
                    'name' => $form->cleanSimpleText($_POST['name']),
                    'price' => $form->cleanNumber($_POST['price']),
                    'type' => (int) $_POST['type'],
                );
                switch ($_POST['type']) {
                    case 1:
                        $data['disk_size'] = $form->cleanNumber($_POST['disk_size']);
                        break;
                    case 2:
                        $data['book_weight'] = $form->cleanNumber($_POST['book_weight']);
                        break;
                    case 3:
                        $data['furniture_height'] = $form->cleanNumber($_POST['furniture_height']);
                        $data['furniture_width'] = $form->cleanNumber($_POST['furniture_width']);
                        $data['furniture_lenght'] = $form->cleanNumber($_POST['furniture_lenght']);
                        break;
                    default:
                        break;
                }

                Container::retrieve('database')->insert(self::TABLE, $data);
                return $this->helper->redirecting('products');
            }
        }
        $add = htmlspecialchars('store');
        return $this->helper->view('add', compact('add', 'form'));
    }
}