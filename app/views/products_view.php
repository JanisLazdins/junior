<h2>PRODUCT LIST</h2>
<form method="POST" action="products" >
    <select name="delete">
        <option value="option" >OPTIONS</option>
        <option value="mass-delete" >Delete Selected</option>
    </select>
    <button type="submit" name="submit2">Submit</button>
    <?php foreach ($products as $product): ?>
        <div>
            <table class="table-design">
                <tr><td><input class="delete-checkbox" type="checkbox" name="checkbox[]" value="<?= $product->getId(); ?>"></td></tr>
                <tr><td colspan="2"> SKU: <?= $product->getSku(); ?></td></tr>
                <tr><td colspan="2"> Name: <?= $product->getName(); ?></td></tr>
                <tr><td colspan="2"> Price: <?= $product->getPrice(); ?></td></tr>
                <tr><td colspan="2"> <?= $product->getAttributes(); ?></td></tr>
            </table>

        </div>
    <?php endforeach; ?>
</form>
