<?php

namespace App\Models;

/**
 * Class contains data unique to Furniture products.
 */
class FurnitreModel extends ProductModel
{

    /**
     * Assigns all properties for Furniture.
     * 
     * @param array $data Contains properties for Furniture object.
     */
    public function __construct($data = [])
    {
        $this->id = $data->id;
        $this->sku = $data->sku;
        $this->name = $data->name;
        $this->price = $data->price;
        $this->furniture_height = $data->furniture_height;
        $this->furniture_width = $data->furniture_width;
        $this->furniture_lenght = $data->furniture_lenght;
        $this->type = $data->type;
    }

    /**
     * Returns unique properties for Furniture.
     *
     * @return string
     */
    public function getAttributes()
    {
        return 'Dimensions: ' . implode(' x ',
                [
                $this->furniture_height,
                $this->furniture_width,
                $this->furniture_lenght,
            ]) . ' cm';
    }
}