<?php

namespace App\Models;

use App\Core\Container;

/**
 * This class contains methods needed in 'add' and 'inspect' views.
 */
class ProductModel
{
    /**
     * Defines name of MySQL database table.
     */
    const TABLE = 'products';

    /**
     * Variable $products Contains unsorted data from database.
     * Each $product is being sorted by the type variable: 1-Disk, 2-Book, 3-Furniture.
     * Products saved in an array $data[] and returned.
     * 
     * @return array
     */
    public function productListings()
    {
        $products = Container::retrieve('database')->getAll(self::TABLE);

        $data = [];
        foreach ($products as $product) {
            if ($product->type == 1) {
                $data[] = new DiskModel($product);
            } elseif ($product->type == 2) {
                $data[] = new BookModel($product);
            } elseif ($product->type == 3) {
                $data[] = new FurnitreModel($product);
            }
        }
        return $data;
    }

    /**
     * Redirect to QueryBuilder class to upload data to 'products' table.
     *
     * @param array $data Contains submitted data to be inserted into database from 'add' view
     */
    public function massDelete($checkbox)
    {
        Container::retrieve('database')->deleteSelected(self::TABLE, $checkbox);
    }

    /**
     * Return product id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Return product Sku
     *
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * Return product Name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Return product Price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price.' EUR';
    }
}