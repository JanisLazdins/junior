<?php

namespace App\Models;

/**
 * Class contains data unique to Book products.
 */
class BookModel extends ProductModel
{

    /**
     * Assigns all properties for Books.
     *
     * @param array $data Contains properties for Book object
     */
    public function __construct($data = [])
    {
        $this->id = $data->id;
        $this->sku = $data->sku;
        $this->name = $data->name;
        $this->price = $data->price;
        $this->book_weight = $data->book_weight;
        $this->type = $data->type;
    }

    /**
     * Returns unique properties for Books.
     *
     * @return string
     */
    public function getAttributes()
    {
        return 'Book weight: ' . $this->book_weight . ' KG';
    }
}