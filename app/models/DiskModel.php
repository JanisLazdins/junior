<?php

namespace App\Models;

/**
 * Class contains data unique to Disk products.
 */
class DiskModel extends ProductModel
{

    /**
     * Assigns all properties for Disks.
     * 
     * @param array $data Contains properties for Disk object.
     */
    public function __construct($data = [])
    {
        $this->id = $data->id;
        $this->sku = $data->sku;
        $this->name = $data->name;
        $this->price = $data->price;
        $this->disk_size = $data->disk_size;
        $this->type = $data->type;
    }

    /**
     * Returns unique properties for Disks.
     *
     * @return string
     */
    public function getAttributes()
    {
        return 'Disk Size: '.$this->disk_size.' MB';
    }
}